using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using F14.Common;

namespace F14
{
    class Projectile : SceneItem
    {
        public SceneItem Parent;
        public Player Player1;
        public Vector2 PosOffset = new Vector2(0, 0);
        public Sounds ShootSound = Sounds.None;
        public int MaxProjectiles = 1;
        public bool IsAimShot = false;
        private float aimSpeed = 1f;

        private bool firstShotFinished = false;

        public Projectile(Game aGame, GraphicsDevice device, Random r,
            Texture2D tex2d, TextureType tt,
            SceneItem veh, Sounds sound, bool isAimShot)
            : base(aGame, device, r, tex2d, tt)
        {
            Parent = veh;
            ShootSound = sound;
            IsAimShot = isAimShot;
            Layer = Parent.Layer + .01f;
            SpeedH = 0f;
            SpeedV = 1f;
            TargetRate = 1f;
        }

        public override void Reset()
        {            
            this.Pos.X = Parent.Pos.X + PosOffset.X;
            this.Pos.Y = Parent.Pos.Y + PosOffset.Y;            
            aimSpeed = this.SpeedV;
            firstShotFinished = false;
            base.Reset();
        }

        protected void AimShotDirection()
        {
            float angle = GetAngle(Player1);
            this.Rotate = angle + MathHelper.ToRadians(180);
            Vector2 direction = new Vector2((float)(-Math.Sin(angle)), (float)(Math.Cos(angle)));
            this.SpeedH = direction.X * -SpeedV;
            this.SpeedV = direction.Y * -SpeedV;
        }

        protected override void UpdateEx(float elapsed, GameTime agt)
        {
            if (IsDestroyed)
                return;

            if (IsAimShot)
            {
                //remove hasfinished check if you want heat seeking projectiles
                if (!firstShotFinished)
                {
                    firstShotFinished = true;
                    AimShotDirection();
                }
                this.Pos.X += SpeedH;
                this.Pos.Y += SpeedV;                
            }
            else
            {
                this.Pos.Y += SpeedV;
            }
        }
    }
}

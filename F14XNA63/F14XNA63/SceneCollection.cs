using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XNAExtras;

namespace F14
{
    class SceneCollection : System.Collections.Generic.List<SceneItem>
    {
        public float TargetRate = 0f;
        public float LastTargetTime = 0f;
        public int MaxItems = 1;

        public bool HasReachedTarget()
        {
            if (LastTargetTime >= TargetRate)
            {
                LastTargetTime = 0;
                return true;
            }
            return false;
        }

        public void Update(float elapsed, GameTime agt)
        {
            Update(elapsed, false, agt);
        }

        public void Update(float elapsed, bool ignoreDestory, GameTime agt)
        {
            if (TargetRate > 0)
                LastTargetTime += elapsed;

            //ai
            foreach (SceneItem si in this)
            {
                if (!si.IsDestroyed | ignoreDestory)
                {
                    si.Update(elapsed, agt);
                }
            }

            //cleanup
            foreach (SceneItem si in this)
            {
                //cleanup expired items
                if (!si.IsDestroyed && !si.Visible)
                {
                    si.IsDestroyed = true;
                }
            }
        }

        public void Draw(SpriteBatch Batch)
        {
            foreach (SceneItem si in this)
            {
                si.Draw(Batch);
            }
        }

        public bool IsAllDestroyed()
        {
            foreach (SceneItem s in this)
            {
                if (!s.IsDestroyed)
                    return false;
            }
            return true;
        }

        public void DestroyAll()
        {
            foreach (SceneItem s in this)
            {
                if (!s.IsDestroyed)
                    s.IsDestroyed = true;
            }
        }
    }
}

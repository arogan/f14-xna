using System;

namespace F14
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (F14Game game = new F14Game())
            {
                game.Run();
            }
        }
    }
}


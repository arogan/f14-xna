using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using F14.Common;

namespace F14
{
    class Enemy : Vehicle
    {
        public Player p1;               
        public bool DoRotate = false;
        private float initDelay = 0f;
        private bool isFirstShot;
        public int Points = 1;
        public override bool IsDestroyed
        {
            get 
            {                 
                if (isDestroyed)
                    return true;
                else
                    return false;                
            }
            set
            {
                isDestroyed = value;
                isFirstShot = !value;
                if (value)
                {
                    lifeTime = 0f;
                }
                else
                {
                    UpdateDelay();
                }
            }
        }

        public Enemy(Game aGame, GraphicsDevice device, Random r, Texture2D tex2d, TextureType tt, Player plyr)
            : base(aGame, device,r,tex2d,tt)
        {
            p1 = plyr;
        }

        public override void Reset()
        {            
            this.Pos.Y = -this.Height;
            UpdateDelay();
            base.Reset();
        }
        
        public void UpdateDelay()
        {
            initDelay = rand.Next(0, 100) / 100f;
        }          
      
        protected override void UpdateMovement(float elapsed,GameTime agt)
        {
            //MovePattern = moveType.Sin;
            //IsAimShot = true;
            switch (MovePattern)
            {
                case moveType.VLine:
                    MoveVLine();
                    break;
                case moveType.Sin:
                    MoveSin(agt);
                    break;
                case moveType.Bounce:
                    MoveBounce();
                    break;
                case moveType.Accelerate:
                    MoveAccelerate(elapsed);
                    break;
                case moveType.SweepRight:
                    MoveSweepRight(elapsed);
                    break;
                case moveType.SweepLeft:
                    MoveSweepLeft(elapsed);
                    break;
                case moveType.BounceBox:
                    MoveBounceBox();
                    break;
            }
            if (DoRotate)
                Rotate = GetAngle(p1);
        }

        protected void MoveSin(GameTime agt)
        {            
            this.Pos.Y += SpeedV;
            this.Pos.X += (float)Math.Sin(Convert.ToDouble(agt.TotalGameTime.TotalSeconds)) * SpeedH;
        }

        protected void MoveAccelerate(float elapsed)
        {
            SpeedV += elapsed * 2f;
            MoveVLine();
        }

        protected void MoveSweepRight(float elapsed)
        {
            this.Pos.X += SpeedH;
            this.Pos.Y += SpeedV;
            if (Pos.Y >= NormalY(15))
            {
                SpeedV += elapsed;
                SpeedH += elapsed * 2.2f;
            }
        }

        protected void MoveSweepLeft(float elapsed)
        {
            this.Pos.X += SpeedH;
            this.Pos.Y += SpeedV;
            if (Pos.Y >= NormalY(15))
            {
                SpeedV += elapsed;
                SpeedH -= elapsed * 2.2f;
            }
        }

        protected int NormalY(float YPercent)
        {
            return actionSafeArea.Top + Convert.ToInt32(actionSafeArea.Height * (YPercent / 100f));
        }

        protected int NormalX(float XPercent)
        {
            return actionSafeArea.Left + Convert.ToInt32(actionSafeArea.Width * (XPercent / 100f));
        }     

        protected void MoveBounce()
        {
            //move the sprite by speed            
            this.Pos.X += SpeedH;
            this.Pos.Y += SpeedV;
            int MaxX = actionSafeArea.Right - Convert.ToInt32(this.BB.Radius);
            int MinX = actionSafeArea.Left;
            int MaxY = actionSafeArea.Bottom - Convert.ToInt32(this.BB.Radius);
            int MinY = actionSafeArea.Top;

            //check for bounce
            if (this.Pos.X > MaxX)
            {
                SpeedH *= -1;
                this.Pos.X = MaxX;
            }
            else if (this.Pos.X < MinX)
            {
                SpeedH *= -1;
                this.Pos.X = MinX;
            }

            if (this.Pos.Y > MaxY)
            {
                SpeedV *= -1;
                this.Pos.Y = MaxY;
            }
            else if (this.Pos.Y < MinY)
            {
                SpeedV *= -1;
                this.Pos.Y = MinY;
            }
        }

        protected void MoveBounceBox()
        {
            //move the sprite by speed            
            this.Pos.X += SpeedH;
            this.Pos.Y += SpeedV;
            int halfX = this.Width / 2;
            int halfY = this.Height / 2;
            int MaxX = actionSafeArea.Right - halfX;
            int MinX = actionSafeArea.Left + halfX;
            int MaxY = actionSafeArea.Bottom - NormalY(50);
            int MinY = actionSafeArea.Top + halfY;

            //check for bounce
            if (this.Pos.X > MaxX)
            {
                SpeedH *= -1;
                this.Pos.X = MaxX;
            }
            else if (this.Pos.X < MinX)
            {
                SpeedH *= -1;
                this.Pos.X = MinX;
            }

            if (this.Pos.Y > MaxY)
            {
                SpeedV *= -1;
                this.Pos.Y = MaxY;
            }
            else if (this.Pos.Y < MinY && this.lifeTime > 4f)
            {
                SpeedV *= -1;
                this.Pos.Y = MinY;
            }
        }

        protected override void UpdateEx(float elapsed, GameTime agt)
        {
            //move enemy
            UpdateMovement(elapsed,agt);

            //update the projectiles              
            foreach (SceneCollection gp in GroupProjectiles)
            {
                gp.Update(elapsed,agt);
                //fire
                this.Fire(gp);
            }
        }

        protected override void Fire(SceneCollection gp)
        {
            if (isFirstShot)
                UpdateDelay();

            if (!this.IsDestroyed && gp.HasReachedTarget() && lifeTime >= initDelay)
            {
                foreach (Projectile m in gp)
                {
                    if (m.IsDestroyed)
                    {      
                        //make sure shot is faster than parent                        
                        if (this.SpeedV > m.SpeedV)
                            m.SpeedV = this.SpeedV + .1f;
                        if (this.SpeedH > m.SpeedH)
                            m.SpeedH = this.SpeedH + .1f;
                        
                        Sound.Play(m.ShootSound);
                        m.Reset();
                        break;
                    }
                }
            }
        }
    } // end class
} // end namespace

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace F14
{
    class SceneItem
    {
        #region properties

        public float lifeTime = 0f;
        public Vector2 Pos = new Vector2(0f, 0f);
        public float Rotate = 0f;
        protected Vector2 Origin;
        //public float Speed = 1f; //global speed that gets muliplied by speed H and V
        public float SpeedH = 0f;
        //public float SpeedH
        //{
        //    get { return speedH * Speed; }
        //    set { speedH = value; }
        //}
        public float SpeedV = 1f;
        //public float SpeedV
        //{
        //    get { return speedV * Speed; }
        //    set { speedV = value; }
        //}
        private Rectangle frame;
        public virtual Rectangle Frame
        {
            get { return frame; }
            set
            {
                frame = value;
                if (hasCollision)
                {
                    updateBB();
                }
                UpdateOrigin();
            }
        }
        private Texture2D t2d;
        public virtual Texture2D T2D
        {
            get { return t2d; }
            set
            {
                t2d = value;
                this.Frame = new Rectangle(0, 0, T2D.Width, T2D.Height);
            }
        }
        public TextureType T2DType;
        protected string tFile;
        public virtual string TFile
        {
            get { return tFile; }
            set { tFile = value; }
        }
        protected Game myGame;
        //protected GameTime myTime;
        protected GraphicsDevice device;
        protected Rectangle actionSafeArea;
        protected Rectangle titleSafeArea;
        public BoundingSphere BB = new BoundingSphere();
        protected bool isDestroyed = true;
        public virtual bool IsDestroyed
        {
            get { return isDestroyed; }
            set
            {
                isDestroyed = value;
                if (value)
                    lifeTime = 0f;
            }
        }

        public bool hasCollision = true;
        private float spriteScale = 1f;
        public float SpriteScale
        {
            get { return spriteScale; }
            set
            {
                spriteScale = value;
                if (hasCollision)
                    updateBB();
            }
        }
        public float Layer = 0f;

        protected Vector3 SCenter = new Vector3(0, 0, 0);
        protected Random rand;
        protected bool HasFinished = false; //used if you want things to happen only once until reset

        public enum visibleType
        {
            All,
            IgnoreTop,
            BottomOnly
        }

        public visibleType VisibleCheck = visibleType.All;

        public enum moveType
        {
            VLine,
            Sin,
            Bounce,
            Accelerate,
            SweepRight,
            SweepLeft,
            BounceBox,
            None
        }
        public moveType MovePattern = moveType.None;

        public enum bbType
        {
            largest,
            smallest
        }
        public bbType BBSize = bbType.largest;

        public Int32 Width
        {
            get { return Convert.ToInt32(Math.Ceiling(this.Frame.Width * SpriteScale)); }
        }

        public Int32 Height
        {
            get { return Convert.ToInt32(Math.Ceiling(this.Frame.Height * SpriteScale)); }
        }

        public bool Visible
        {
            get
            {
                switch (VisibleCheck)
                {
                    case visibleType.All:
                        return this.IsVisible();
                    case visibleType.IgnoreTop:
                        return this.IsVisible(true);
                    case visibleType.BottomOnly:
                        return this.IsVisibleBottom();
                    default:
                        return this.IsVisibleBottom();
                }
            }
        }

        public float TargetRate = -1f;
        protected float LastTargetTime = 0f;


        #endregion

        #region Constructors

        public SceneItem(Game aGame, GraphicsDevice aDevice)
        {
            myGame = aGame;            
            device = aDevice;
            int bbWidth = device.PresentationParameters.BackBufferWidth;
            int bbHeight = device.PresentationParameters.BackBufferHeight;
#if XBOX360
            int dy = (int)(0.05 * bbHeight);
            int dx = (int)(0.05 * bbWidth);
            actionSafeArea = new Rectangle(dx, dy, bbWidth - 2 * dx, bbHeight - 2 * dy);
            dy = (int)(0.1 * bbHeight);
            dx = (int)(0.1 * bbWidth);
            titleSafeArea = new Rectangle(dx, dy, bbWidth - 2 * dx, bbHeight - 2 * dy);
#else
            actionSafeArea = new Rectangle(0, 0, bbWidth, bbHeight);
            int dy = 10;
            int dx = 10;
            titleSafeArea = new Rectangle(dx, dy, bbWidth - 2 * dx, bbHeight - 2 * dy);
#endif
        }

        public SceneItem(Game aGame, GraphicsDevice aDevice, Random r)
            : this(aGame, aDevice)
        {
            rand = r;
        }

        public SceneItem(Game aGame, GraphicsDevice aDevice, Random r, Texture2D tex2d, TextureType tt)
            : this(aGame, aDevice, r)
        {
            T2D = tex2d;
            T2DType = tt;
            if (object.Equals(this.Frame, null) || (this.Frame.Width == 0 && this.Frame.Height == 0))
                this.Frame = new Rectangle(0, 0, T2D.Width, T2D.Height);
            if (hasCollision)
                updateBB(Pos.X, Pos.Y);
        }

        /// <summary>
        /// call this when you are ready to activate an object so it starts to participate in the scene
        /// </summary>
        public virtual void Reset()
        {
            LastTargetTime = 0f;
            lifeTime = 0f;
            IsDestroyed = false;
            HasFinished = false;
            UpdateOrigin();
            if (hasCollision)
                updateBB();
        }

        #endregion

        public void UpdateOrigin()
        {
            Origin.X = this.Frame.Width / 2;
            Origin.Y = this.Frame.Height / 2;
        }      

        public virtual bool IsVisibleBottom()
        {
            if (this.Pos.Y - BB.Radius > device.PresentationParameters.BackBufferHeight)
                return false;
            else
                return true;
        }

        public virtual bool IsVisible(bool ignoreTop)
        {
            if (this.Pos.Y - BB.Radius > device.PresentationParameters.BackBufferHeight)
                return false;
            if (!ignoreTop)
            {
                if (this.Pos.Y + BB.Radius < 0)
                    return false;
            }
            if (this.Pos.X - BB.Radius > device.PresentationParameters.BackBufferWidth)
                return false;
            if (this.Pos.X + BB.Radius < 0)
                return false;
            return true;
        }
        public virtual bool IsVisible()
        {
            return IsVisible(false);
        }

        //called during update loop
        protected void updateBB(float x, float y)
        {
            SCenter.X = x;
            SCenter.Y = y;
            BB.Center = SCenter;
        }

        //initalize bb size
        public void updateBB()
        {
            if (BBSize == bbType.largest)
            {
                if (this.Width >= this.Height)
                    BB.Radius = this.Width / 2f;
                else
                    BB.Radius = this.Height / 2f;
            }
            else
            {
                updateBBSmall();
            }
            updateBB(this.Pos.X, this.Pos.Y);
        }

        //initalize bb size
        public void updateBBSmall()
        {
            if (this.Width >= this.Height)
                BB.Radius = this.Height / 2f;
            else
                BB.Radius = this.Width / 2f;
        }

        protected virtual bool HasReachedTarget()
        {
            if (LastTargetTime >= TargetRate)
            {
                LastTargetTime = 0;
                return true;
            }
            return false;
        }

        public void MoveVLine()
        {
            SpeedH = 0;
            this.Pos.Y += SpeedV;
        }

        protected float GetAngle(SceneItem obj)
        {
            float angle = (float)System.Math.Atan2(this.Pos.Y - obj.Pos.Y, this.Pos.X - obj.Pos.X);
            angle = angle - MathHelper.ToRadians(90);
            return angle;
        }

        public void Update(float elapsed, GameTime agt)
        {
            if (HasFinished)
                return;
            if (TargetRate > 0)
                LastTargetTime += elapsed;

            if (!IsDestroyed)
                lifeTime += elapsed;

            UpdateEx(elapsed, agt);

            //do this last  
            if (hasCollision)
                updateBB(Pos.X, Pos.Y);
        }

        protected virtual void UpdateMovement(float elapsed, GameTime agt)
        {
            if (MovePattern == moveType.VLine)
            {
                MoveVLine();
            }
        }

        protected virtual void UpdateEx(float elapsed, GameTime agt)
        {
            UpdateMovement(elapsed,agt);
        }

        public virtual void Draw(SpriteBatch Batch)
        {
            if (IsDestroyed)
                return;

            Batch.Draw(this.T2D, this.Pos, this.Frame, Color.White, Rotate, Origin, SpriteScale, SpriteEffects.None, Layer);
        }
    } //end SceneItem class  
}

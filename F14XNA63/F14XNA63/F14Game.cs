
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Storage;
using XNAExtras;
using F14.Common;
using SimpleInput;
#endregion

namespace F14
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class F14Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        ContentManager content;

        private Random rand = new Random();
        private Director myDirector;

        SpriteBatch spriteBatch;
        public static BitmapFont m_fontTimes;

        public F14Game()
        {
            graphics = new GraphicsDeviceManager(this);
            content = new ContentManager(Services);
           
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            SimpleInput.IM InputManager;
            
            InputManager = new SimpleInput.IM(this);
            Components.Add(InputManager);                   
        }


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            //Initialise the sound
            Sound.Initialize();

            if (!graphics.IsFullScreen)
            {
                Window.Title = "F14 XNA";
                
                graphics.PreferredBackBufferHeight = 768;
                graphics.PreferredBackBufferWidth = 1024;

                graphics.ApplyChanges();
            }

            m_fontTimes = new BitmapFont(@"Content\Fonts\times.xml", content);
            
            myDirector = new Director(this, new GameTime(), graphics.GraphicsDevice, rand, m_fontTimes);

            base.Initialize();
        }


        /// <summary>
        /// Load your graphics content.  If loadAllContent is true, you should
        /// load content from both ResourceManagementMode pools.  Otherwise, just
        /// load ResourceManagementMode.Manual content.
        /// </summary>
        /// <param name="loadAllContent">Which type of content to load.</param>
        protected override void LoadContent()
        {           
            // TODO: Load any ResourceManagementMode.Manual content
            myDirector.LoadResources(content);

            spriteBatch = new SpriteBatch(graphics.GraphicsDevice);

            //fonts
            m_fontTimes.Reset(graphics.GraphicsDevice, spriteBatch);
        }


        /// <summary>
        /// Unload your graphics content.  If unloadAllContent is true, you should
        /// unload content from both ResourceManagementMode pools.  Otherwise, just
        /// unload ResourceManagementMode.Manual content.  Manual content will get
        /// Disposed by the GraphicsDevice during a Reset.
        /// </summary>
        /// <param name="unloadAllContent">Which type of content to unload.</param>
        protected override void UnloadContent()
        {
            content.Unload();            
        }


        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the default game to exit on Xbox 360 and Windows
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            // The time since Update was called last
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //call update on all scene items                
            CheckForExit();

            Sound.Update();
            myDirector.Update(elapsed, gameTime);

            // Let the GameComponents update
            //UpdateComponents();

            base.Update(gameTime);
        }

        private void CheckForExit()
        {
            if (IM.IsKeyDown(Keys.Escape, true) || IM.ControllerBackBtnDown(PlayerIndex.One))
                this.Exit();
            if (IM.IsKeyDown(Keys.Home, true) || IM.ControllerStartBtnDown(PlayerIndex.One))
                this.myDirector.Reset();

        }

        public void drawText(int x, int y, string str)
        {
            m_fontTimes.KernEnable = true;
            int nWidth = m_fontTimes.MeasureString(str);
            m_fontTimes.DrawString(x, y, Color.Yellow, str, nWidth);
        }

        public void drawTextCenter(string str)
        {
            m_fontTimes.KernEnable = true;
            int nWidth = m_fontTimes.MeasureString(str);
            int backbufferWidth = graphics.GraphicsDevice.PresentationParameters.BackBufferWidth;
            m_fontTimes.DrawString(( backbufferWidth - nWidth) / 2, (Window.ClientBounds.Height - m_fontTimes.LineHeight) / 2, Color.Yellow, str, nWidth);
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here            
            //spriteBatch.Begin(SpriteSortMode.FrontToBack, new BlendState());
            spriteBatch.Begin();

            myDirector.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
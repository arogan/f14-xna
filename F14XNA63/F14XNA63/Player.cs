using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using F14.Common;
using SimpleInput;

namespace F14
{
    class Player : Vehicle
    {
        private Rectangle rNeutral = new Rectangle(50, 0, 50, 80);
        private Rectangle rLeft = new Rectangle(0, 0, 50, 80);
        private Rectangle rRight = new Rectangle(100, 0, 50, 80);

        public int lives = 3;
        public int totalLives = 3;
        public Texture2D lifeTex;

        private Rumble rumble;

        public override bool IsDestroyed
        {
            get { return isDestroyed; }
            set
            {
                isDestroyed = value;                
                if (value)
                {                    
                    lifeTime = 0f;                    
                }                
            }
        }

        public Player(Game aGame, GraphicsDevice aDevice, Random r, Texture2D tex2d, TextureType tt, Texture2D lifet)
            : base(aGame, aDevice, r, tex2d, tt)
        {
            Layer = .1f;            
            SpeedV = 6f;
            SpeedH = 6f;
            this.Frame = rNeutral;
            lifeTex = lifet;
            lives = totalLives;            
            rumble = new Rumble(myGame, device, .5f);
        }

        //return true if no more lives
        public bool DoDestroy()
        {
            if (this.IsDestroyed)
            {
                lives--;
                rumble.Reset();
            }
            if (lives > 0)
            {
                this.Reset();
            }
            else
            {
                return true;
            }
            return false;
        }

        public override void Reset()
        {
            this.Pos.X = device.PresentationParameters.BackBufferWidth / 2f;
            this.Pos.Y = device.PresentationParameters.BackBufferHeight - 100;
            base.Reset();
        }

        protected override void UpdateEx(float elapsed, GameTime agt)
        {
            base.UpdateEx(elapsed, agt);

            //movement
            this.Frame = rNeutral;
            if (IM.IsKeyDown(Keys.W, true) || IM.ControllerLeftStick(PlayerIndex.One).Y > 0 || IM.ControllerUpBtnDown(PlayerIndex.One))
            {
                this.Pos.Y -= this.SpeedV;
                //this.Frame = rUp;
            }
            if (IM.IsKeyDown(Keys.S, true) || IM.ControllerLeftStick(PlayerIndex.One).Y < 0 || IM.ControllerDownBtnDown(PlayerIndex.One))
            {
                this.Pos.Y += this.SpeedV;
                //this.Frame = rDown;
            }
            if (IM.IsKeyDown(Keys.A, true) || IM.ControllerLeftStick(PlayerIndex.One).X < 0 || IM.ControllerLeftBtnDown(PlayerIndex.One))
            {
                this.Pos.X -= this.SpeedH;
                this.Frame = rLeft;
            }
            if (IM.IsKeyDown(Keys.D, true) || IM.ControllerLeftStick(PlayerIndex.One).X > 0 || IM.ControllerRightBtnDown(PlayerIndex.One))
            {
                this.Pos.X += this.SpeedH;
                this.Frame = rRight;
            }
            if (IM.IsKeyDown(Keys.Space, true) || IM.ControllerABtnDown(PlayerIndex.One) || IM.ControllerRightTrigger(PlayerIndex.One) > 0)
            {
                //fire                     
                Fire(GroupProjectiles[0]);
            }

            //player bounds check
            if (this.Pos.X + Width / 2 >= actionSafeArea.Right)
                this.Pos.X = actionSafeArea.Right - Width / 2;
            if (this.Pos.X - Width / 2 <= actionSafeArea.Left)
                this.Pos.X = actionSafeArea.Left + Width / 2;
            if (this.Pos.Y + Height / 2 >= actionSafeArea.Bottom)
                this.Pos.Y = actionSafeArea.Bottom - Height / 2;
            if (this.Pos.Y - Height / 2 <= actionSafeArea.Top)
                this.Pos.Y = actionSafeArea.Top + Height / 2;

            rumble.Update(elapsed, agt);
        }

        public override void Draw(SpriteBatch Batch)
        {
            base.Draw(Batch);

            //draw lives
            int x = titleSafeArea.Right - lifeTex.Width;
            int y = titleSafeArea.Bottom - lifeTex.Height;
            for (int i = 0; i < lives - 1; i++)
            {
                Batch.Draw(lifeTex, new Vector2(x, y), Color.White);
                x -= lifeTex.Width;
            }
        }
    }
}

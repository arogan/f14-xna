using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using F14.Common;

namespace F14
{
    class Vehicle : SceneItem
    {
        private Rectangle rNeutral;
        //the number of items in group projectiles should mach the number of items in alphaprojectile
        //alpha projectile is used as a template for creating the series of projectiles in each group
        //a projectile groups represents a single type of projectile like bullets, missles, or plasma
        //multiple groups means your vehicle can shoot multiple projectile types each patterened after 
        //an alpha projectile.
        public System.Collections.Generic.List<SceneCollection> GroupProjectiles = new List<SceneCollection>();
        public System.Collections.Generic.List<Projectile> AlphaProjectiles = new List<Projectile>();
        public int MaxProjectiles = 3;
        public int Health = 1;

        public Vehicle(Game aGame, GraphicsDevice device, Random r, Texture2D tex2d, TextureType tt)
            : base(aGame, device, r, tex2d, tt)
        {
            Layer = .5f;
        }

        public override void Reset()
        {            
            CreateProjectiles();
            base.Reset();
        }

        public virtual void CreateProjectiles()
        {
            GroupProjectiles.Clear();
            foreach (Projectile p1 in AlphaProjectiles)
            {
                SceneCollection sc = new SceneCollection();
                for (int i = 0; i < p1.MaxProjectiles; i++)
                {
                    Projectile pNew = new Projectile(myGame, device, rand, p1.T2D, p1.T2DType,
                        this, p1.ShootSound, p1.IsAimShot);                    
                    pNew.Player1 = p1.Player1;
                    //pNew.Speed = p1.Speed;
                    pNew.SpeedH = p1.SpeedH;
                    pNew.SpeedV = p1.SpeedV;
                    pNew.SpriteScale = p1.SpriteScale;
                    pNew.MaxProjectiles = p1.MaxProjectiles;
                    pNew.PosOffset = p1.PosOffset;

                    sc.Add(pNew);
                }
                sc.TargetRate = p1.TargetRate;
                GroupProjectiles.Add(sc);
            }
        }

        protected override void UpdateEx(float elapsed, GameTime agt)
        {            
            //update the projectiles              
            foreach (SceneCollection gp in GroupProjectiles)
            {
                gp.Update(elapsed, agt);
                //fired called in a derrived class                
            }
        }

        protected virtual void Fire(SceneCollection gp)
        {
            if (!IsDestroyed && gp.HasReachedTarget())
            {
                foreach (Projectile m in gp)
                {
                    if (m.IsDestroyed)
                    {
                        Sound.Play(m.ShootSound);
                        m.Reset();
                        break;
                    }
                }
            }
        }

        public bool AreAllProjectilesDestoryed()
        {            
            foreach (SceneCollection gp in GroupProjectiles)
            {
                if (!gp.IsAllDestroyed())
                {
                    return false;
                }                
            }
            return true;
        }

        public void DestroyAllProjectiles()
        {

            foreach (SceneCollection gp in GroupProjectiles)
            {
                gp.DestroyAll();
            }            
        }

        public override void Draw(SpriteBatch Batch)
        {
            foreach (SceneCollection gp in GroupProjectiles)
            {
                gp.Draw(Batch);
            }
            base.Draw(Batch);
        }
    }
}

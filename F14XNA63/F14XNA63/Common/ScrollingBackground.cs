using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
//using Microsoft.Xna.Framework.Components;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Storage;

namespace F14.Common
{
    /// <summary>
    /// Basic idea is to draw all textures stacked edge to edge on top of each other starting with
    /// index 0 at the bottom.  We duplicate the bottom texture at the top to help with wrapping
    /// then we just change the vector y position over time to scroll.    
    /// </summary>
    public class ScrollingBackground
    {        
        public float speed = 1;
        public int Direction = 1;
        public float Layer = 0;
        private Texture2D[] textures;
        private int tIndex = 0;        
        private int tMax = 0;
        private int screenheight, screenwidth;                                 
        private int x;
        private int tw;
        private float scaleX = 1;        
        private int moveY = 0;        
        private int lastPosY = 0;
        private int totalHeight = 0;
        private Rectangle Frame = new Rectangle();
        private Vector2 Pos = new Vector2(0,0);
        private Vector2 Origin = new Vector2(0, 0);
        private Game myGame;

        // ScrollingBackground.Load
        public ScrollingBackground(Game aGame, GraphicsDevice Device, params Texture2D[] ts)
        {
            myGame = aGame;
            tMax = ts.GetUpperBound(0);
            tMax++;
            textures = new Texture2D[tMax + 1];
            for (int i = 0; i < ts.Length;i++ )
            {
                textures[i] = ts[i];
                totalHeight = totalHeight + textures[i].Height;
            }
            //repeat first texture in last index for looping
            textures[tMax] = textures[0];
            screenheight = Device.PresentationParameters.BackBufferHeight;
            screenwidth = Device.PresentationParameters.BackBufferWidth;
            //screenheight = myGame.Window.ClientBounds.Height;
            //screenwidth = myGame.Window.ClientBounds.Width;

            tw = textures[0].Width;   
            if (screenwidth > tw)
            {
                scaleX = (float)screenwidth / (float)tw;
            }
            
            //init x and y
            if (scaleX > 1)
            {
                x = 0;
            }
            else
            {
                x = (tw / 2) - (screenwidth / 2);
            }
            Pos.X = 0;
            Frame.X = x;
            Frame.Y = 0;
            Frame.Width = screenwidth;
            if (Direction == -1)
                tIndex = tMax;            
        }
        
        // ScrollingBackground.Update
        public void Update(float elapsed)
        {
            moveY -= Convert.ToInt32(speed);
            if (moveY < totalHeight * -1)
                    moveY = 0;          
        }
       
        // ScrollingBackground.Draw
        public void Draw(SpriteBatch Batch)
        {
                       
            if (Direction == 1)
            {      
                //draw all textures stacked end to end but change position (moveY) over time    
                lastPosY = screenheight;    
                for (int i = 0; i <= tMax; i++)
                {
               
                    Pos.Y = lastPosY - this.textures[tIndex].Height - moveY;
                    if (Pos.Y <= screenheight && Pos.Y >= -(screenheight + this.textures[tIndex].Height))
                    {
                        Frame.Height = this.textures[tIndex].Height;
                        Batch.Draw(this.textures[tIndex],
                        Pos,
                        Frame,
                        Color.White,
                        0f, Origin, scaleX, SpriteEffects.None, Layer);
                    }
                    lastPosY = lastPosY - (this.textures[tIndex].Height);                    
                    tIndex++;
                    if (tIndex > tMax)
                    {
                        tIndex = 0;
                    }
                }         
            }
            else
            {
                //draw all textures stacked end to end but change position (moveY) over time    
                lastPosY = 0;
                for (int i = 0; i <= tMax; i++)
                {

                    Pos.Y = lastPosY + moveY;
                    if (Pos.Y <= screenheight && Pos.Y >= -(screenheight + this.textures[tIndex].Height))
                    {
                        Frame.Height = this.textures[tIndex].Height;
                        Batch.Draw(this.textures[tIndex],
                        Pos,
                        Frame,
                        Color.White,
                        0f, Origin, scaleX, SpriteEffects.None, Layer);
                    }
                    lastPosY = lastPosY + (this.textures[tIndex].Height);
                    tIndex--;
                    if (tIndex < 0)
                    {
                        tIndex = tMax;
                    }
                }         
            }
        }
    }

}

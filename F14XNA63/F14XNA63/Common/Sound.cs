using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework.Audio;

namespace F14.Common
{
    /// <summary>
    /// An enum for all of the spaceWar sounds
    /// </summary>
    public enum Sounds
    {
        /// <summary>
        /// Peashoot gun shot sound
        /// </summary>
        PeashooterFire,       
        RocketExplode,      
        /// Double machine gun weapon sound
        /// </summary>
        DoubleMachineGunFire,
        /// <summary>
        /// BFG Explosion sound
        /// </summary>    
        ExplodeShip,
        gameover,
        getready,
        warning2,
        None,
    }

    /// <summary>
    /// Abstracts away the sounds for a simple interface using the Sounds enum
    /// </summary>
    public static class Sound
    {
        
        private static AudioEngine engine;
        private static WaveBank wavebank;
        private static SoundBank soundbank;

        private static string[] cueNames = new string[]
        {
            "PeashooterFire",             // 
            "RocketExplode",  //
            "DoubleMachineGunFire",    //
            "ExplodeShip",
            "gameover",
            "getready",
            "warning2"         
        };

        /// <summary>
        /// Plays a sound
        /// </summary>
        /// <param name="sound">Which sound to play</param>
        /// <returns>XACT cue to be used if you want to stop this particular looped sound. Can be ignored for one shot sounds</returns>
        public static void Play(Sounds sound)
        {
            if (sound == Sounds.None)
                return;
            soundbank.PlayCue(cueNames[(int)sound].ToString());            
        }

        /// <summary>
        /// Stops a previously playing cue
        /// </summary>
        /// <param name="cue">The cue to stop that you got returned from Play(sound)</param>
        public static void Stop(Cue cue)
        {
            cue.Stop(AudioStopOptions.Immediate);
        }

        /// <summary>
        /// Starts up the sound code
        /// </summary>
        public static void Initialize()
        {
            engine = new AudioEngine(@"Content\Sounds\f14.xgs");            
            wavebank = new WaveBank(engine, @"Content\Sounds\Wave Bank.xwb");
            soundbank = new SoundBank(engine, @"Content\Sounds\Sound Bank.xsb");
        }

        public static void Update()
        {
            engine.Update();
        }

        /// <summary>
        /// Shuts down the sound code tidily
        /// </summary>
        public static void Shutdown()
        {
            soundbank.Dispose();
            wavebank.Dispose();
            engine.Dispose();
        }
    }
}

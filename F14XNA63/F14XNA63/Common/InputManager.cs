using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
//using Microsoft.Xna.Framework.Components;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Storage;

namespace SimpleInput
{
    public class IM: Microsoft.Xna.Framework.GameComponent
    {
        // Variables for Keyboard
        private static KeyboardState CurrentKeyState;
        private static List<Keys> KeysPressedLastFrame = new List<Keys>();
#if !XBOX360
        // Variables for mouse
        private static MouseState CurrentMouseState;
        private static MouseState LastMouseState;
#endif
        // Variables for the controller
        private static GamePadState[] CurrentGamePadState = new GamePadState[4];
        private static GamePadState[] LastGamePadState = new GamePadState[4];

        public IM(Game game) : base(game)
        {            
        //    InitializeComponent();
        }

        public override void Initialize()
        {
            // Get the current key state
            CurrentKeyState = Keyboard.GetState();
#if !XBOX360
            // Set the mouse window
            Mouse.WindowHandle = this.Game.Window.Handle;

            // Set starting state
            CurrentMouseState = LastMouseState = Mouse.GetState();
#endif
            // Load Starting States
            CurrentGamePadState[(int)PlayerIndex.One] = LastGamePadState[(int)PlayerIndex.One] = GamePad.GetState(PlayerIndex.One);
            CurrentGamePadState[(int)PlayerIndex.Two] = LastGamePadState[(int)PlayerIndex.Two] = GamePad.GetState(PlayerIndex.Two);
            CurrentGamePadState[(int)PlayerIndex.Three] = LastGamePadState[(int)PlayerIndex.Three] = GamePad.GetState(PlayerIndex.Three);
            CurrentGamePadState[(int)PlayerIndex.Four] = LastGamePadState[(int)PlayerIndex.Four] = GamePad.GetState(PlayerIndex.Four);
        }

        public override void Update(GameTime gameTime)
        {
            // Clear old list of keys down
            KeysPressedLastFrame.Clear();

            // Save the last frames keys
            Keys[] KeysDown = CurrentKeyState.GetPressedKeys();

            // Add Keys to last frames down
            for (int i = 0; i < KeysDown.Length; i++)
                KeysPressedLastFrame.Add(KeysDown[i]);

            // Get Current state of the keys
            CurrentKeyState = Keyboard.GetState();
#if !XBOX360
            // Save last mouse state
            LastMouseState = CurrentMouseState;

            // Get the current state
            CurrentMouseState = Mouse.GetState();
#endif
            // Save the last controller state
            LastGamePadState[(int)PlayerIndex.One] = CurrentGamePadState[(int)PlayerIndex.One];
            LastGamePadState[(int)PlayerIndex.Two] = CurrentGamePadState[(int)PlayerIndex.Two];
            LastGamePadState[(int)PlayerIndex.Three] = CurrentGamePadState[(int)PlayerIndex.Three];
            LastGamePadState[(int)PlayerIndex.Four] = CurrentGamePadState[(int)PlayerIndex.Four];

            // Get the current states
            CurrentGamePadState[(int)PlayerIndex.One] = GamePad.GetState(PlayerIndex.One);
            CurrentGamePadState[(int)PlayerIndex.Two] = GamePad.GetState(PlayerIndex.Two);
            CurrentGamePadState[(int)PlayerIndex.Three] = GamePad.GetState(PlayerIndex.Three);
            CurrentGamePadState[(int)PlayerIndex.Four] = GamePad.GetState(PlayerIndex.Four);
        }
                
        // Check to see if a key is down
        // Repeat - do you want repeating presses
        public static bool IsKeyDown(Keys CheckKey, bool Repeat)
        {
            // Get the state of the key
            bool KeyIsDown = CurrentKeyState.IsKeyDown(CheckKey);

            // Do we need to check for repeats?
            if (KeyIsDown && !Repeat)
            {
                return !WasKeyDown(CheckKey);
            }
            else
                return KeyIsDown;
        }

        public static bool IsKeyDown(Keys CheckKey)
        {
            return IsKeyDown(CheckKey, true);
        }

        // Was the key down?
        public static bool WasKeyDown(Keys CheckKey)
        {
            // Get the state of the key
            bool KeyIsDown = false;

            foreach (Keys CurKey in KeysPressedLastFrame)
            {
                if (CheckKey == CurKey)
                    return true;
            }

            return KeyIsDown;
        }
#if !XBOX360
        // Is the Left Mouse Button Down
        // Repeat - do you want repeating presses
        public static bool MouseLeftBtnDown(bool Repeat)
        {
            bool ButtonDown = (CurrentMouseState.LeftButton == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasMouseLeftBtnDown();
            }
            else
                return ButtonDown;
        }

        public static bool MouseLeftBtnDown()
        {
            return MouseLeftBtnDown(true);
        }

        // Was the Left Mouse Button Down
        public static bool WasMouseLeftBtnDown()
        {
            return (LastMouseState.LeftButton == ButtonState.Pressed);
        }

        // Is the Right Mouse Button Down
        // Repeat - do you want repeating presses
        public static bool MouseRightBtnDown(bool Repeat)
        {
            bool ButtonDown = (CurrentMouseState.RightButton == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasMouseRightBtnDown();
            }
            else
                return ButtonDown;
        }

        public static bool MouseRightBtnDown()
        {
            return MouseRightBtnDown(true);
        }

        // Was the Right Mouse Button Down
        public static bool WasMouseRightBtnDown()
        {
            return (LastMouseState.RightButton == ButtonState.Pressed);
        }

        // Is the Middle Mouse Button Down
        // Repeat - do you want repeating presses
        public static bool MouseMiddleBtnDown(bool Repeat)
        {
            bool ButtonDown = (CurrentMouseState.MiddleButton == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasMouseMiddleBtnDown();
            }
            else
                return ButtonDown;
        }

        public static bool MouseMiddleBtnDown()
        {
            return MouseMiddleBtnDown(true);
        }

        // Was the Middle Mouse Button Down
        public static bool WasMouseMiddleBtnDown()
        {
            return (LastMouseState.MiddleButton == ButtonState.Pressed);
        }

        // Is the XButton1 Mouse Button Down
        // Repeat - do you want repeating presses
        public static bool MouseXButton1BtnDown(bool Repeat)
        {
            bool ButtonDown = (CurrentMouseState.XButton1 == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasMouseXButton1BtnDown();
            }
            else
                return ButtonDown;
        }

        // Was the XButton1 Mouse Button Down
        public static bool WasMouseXButton1BtnDown()
        {
            return (LastMouseState.XButton1 == ButtonState.Pressed);
        }

        // Is the XButton2 Mouse Button Down
        // Repeat - do you want repeating presses
        public static bool MouseXButton2BtnDown(bool Repeat)
        {
            bool ButtonDown = (CurrentMouseState.XButton2 == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasMouseXButton2BtnDown();
            }
            else
                return ButtonDown;
        }

        // Was the XButton1 Mouse Button Down
        public static bool WasMouseXButton2BtnDown()
        {
            return (LastMouseState.XButton2 == ButtonState.Pressed);
        }

        // What is the position of the mouse
        // Returns absolute position
        public static Vector2 MousePosition()
        {
            Vector2 MousePosition;
            MousePosition.X = CurrentMouseState.X;
            MousePosition.Y = CurrentMouseState.Y;
            return MousePosition;
        }

        // Returns relative position
        public static Vector2 MouseRelativePosition()
        {
            Vector2 MousePosition;
            MousePosition.X = CurrentMouseState.X - LastMouseState.X;
            MousePosition.Y = CurrentMouseState.Y - LastMouseState.Y;
            return MousePosition;
        }

        // Get X Position
        public static int MouseXPosition()
        {
            return CurrentMouseState.X;
        }

        // Get Y Position
        public static int MouseYPosition()
        {
            return CurrentMouseState.Y;
        }

        // Get Scroll Position
        public static int MouseScrollPosition()
        {
            return CurrentMouseState.ScrollWheelValue;
        }

        // Get Relative X Position
        public static int MouseRelativeXPosition()
        {
            return CurrentMouseState.X - LastMouseState.X;
        }

        // Get Relative Y Position
        public static int MouseRelativeYPosition()
        {
            return CurrentMouseState.Y - LastMouseState.Y;
        }

        // Get Relative Scroll Position
        public static int MouseRelativeScrollPosition()
        {
            return CurrentMouseState.ScrollWheelValue - LastMouseState.ScrollWheelValue;
        }

        // Get Last X Position
        public static int MouseLastXPosition()
        {
            return LastMouseState.X;
        }

        // Get Last Y Position
        public static int MouseLastYPosition()
        {
            return LastMouseState.Y;
        }

        // Get Last Scroll Position
        public static int MouseLastScrollPosition()
        {
            return LastMouseState.ScrollWheelValue;
        }
#endif
        // Is the Controller connected
        public static bool ControllerConnected(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].IsConnected;
        }

        public static bool ControllerConnectedThisFrame(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].IsConnected && !LastGamePadState[(int)CurIndex].IsConnected;
        }

        // Is the Controller A Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerABtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.A == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerABtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerABtnDown(PlayerIndex CurIndex)
        {
            return ControllerABtnDown(CurIndex, true);
        }

        // Was the Controller A Button Down
        public static bool WasControllerABtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.A == ButtonState.Pressed);
        }

        // Is the Controller B Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerBBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.B == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerBBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerBBtnDown(PlayerIndex CurIndex)
        {
            return ControllerBBtnDown(CurIndex, true);
        }

        // Was the Controller B Button Down
        public static bool WasControllerBBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.B == ButtonState.Pressed);
        }

        // Is the Controller X Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerXBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.X == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerXBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerXBtnDown(PlayerIndex CurIndex)
        {
            return ControllerXBtnDown(CurIndex, true);
        }

        // Was the Controller X Button Down
        public static bool WasControllerXBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.X == ButtonState.Pressed);
        }

        // Is the Controller Y Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerYBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.Y == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerYBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerYBtnDown(PlayerIndex CurIndex)
        {
            return ControllerYBtnDown(CurIndex, true);
        }

        // Was the Controller Y Button Down
        public static bool WasControllerYBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.Y == ButtonState.Pressed);
        }

        // Is the Controller Back Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerBackBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.Back == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerBackBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerBackBtnDown(PlayerIndex CurIndex)
        {
            return ControllerBackBtnDown(CurIndex, true);
        }

        // Was the Controller Back Button Down
        public static bool WasControllerBackBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.Back == ButtonState.Pressed);
        }

        // Is the Controller Start Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerStartBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.Start == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerStartBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerStartBtnDown(PlayerIndex CurIndex)
        {
            return ControllerStartBtnDown(CurIndex, true);
        }

        // Was the Controller Start Button Down
        public static bool WasControllerStartBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.Start == ButtonState.Pressed);
        }

        // Is the Controller LeftShoulder Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerLeftShoulderBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.LeftShoulder == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerLeftShoulderBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerLeftShoulderBtnDown(PlayerIndex CurIndex)
        {
            return ControllerLeftShoulderBtnDown(CurIndex, true);
        }

        // Was the Controller LeftShoulder Button Down
        public static bool WasControllerLeftShoulderBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.LeftShoulder == ButtonState.Pressed);
        }

        // Is the Controller RightShoulder Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerRightShoulderBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.RightShoulder == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerRightShoulderBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerRightShoulderBtnDown(PlayerIndex CurIndex)
        {
            return ControllerRightShoulderBtnDown(CurIndex, true);
        }

        // Was the Controller RightShoulder Button Down
        public static bool WasControllerRightShoulderBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.RightShoulder == ButtonState.Pressed);
        }

        // Is the Controller LeftStick Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerLeftStickBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.LeftStick == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerLeftStickBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerLeftStickBtnDown(PlayerIndex CurIndex)
        {
            return ControllerLeftStickBtnDown(CurIndex, true);
        }

        // Was the Controller Y Button Down
        public static bool WasControllerLeftStickBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.LeftStick == ButtonState.Pressed);
        }

        // Is the Controller RightStick Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerRightStickBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].Buttons.RightStick == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerRightStickBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerRightStickBtnDown(PlayerIndex CurIndex)
        {
            return ControllerRightStickBtnDown(CurIndex, true);
        }

        // Was the Controller Y Button Down
        public static bool WasControllerRightStickBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].Buttons.RightStick == ButtonState.Pressed);
        }

        // Is the Controller Left Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerLeftBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].DPad.Left == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerLeftBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerLeftBtnDown(PlayerIndex CurIndex)
        {
            return ControllerLeftBtnDown(CurIndex, true);
        }

        // Was the Controller Left Button Down
        public static bool WasControllerLeftBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].DPad.Left == ButtonState.Pressed);
        }

        // Is the Controller Right Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerRightBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].DPad.Right == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerRightBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerRightBtnDown(PlayerIndex CurIndex)
        {
            return ControllerRightBtnDown(CurIndex, true);
        }

        // Was the Controller Right Button Down
        public static bool WasControllerRightBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].DPad.Right == ButtonState.Pressed);
        }

        // Is the Controller Up Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerUpBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].DPad.Up == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerUpBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerUpBtnDown(PlayerIndex CurIndex)
        {
            return ControllerUpBtnDown(CurIndex, true);
        }

        // Was the Controller Up Button Down
        public static bool WasControllerUpBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].DPad.Up == ButtonState.Pressed);
        }

        // Is the Controller Down Button Down
        // Repeat - do you want repeating presses
        public static bool ControllerDownBtnDown(PlayerIndex CurIndex, bool Repeat)
        {
            bool ButtonDown = (CurrentGamePadState[(int)CurIndex].DPad.Down == ButtonState.Pressed);

            // Do we need to check for repeats?
            if (ButtonDown && !Repeat)
            {
                return !WasControllerDownBtnDown(CurIndex);
            }
            else
                return ButtonDown;
        }

        public static bool ControllerDownBtnDown(PlayerIndex CurIndex)
        {
            return ControllerDownBtnDown(CurIndex, true);
        }

        // Was the Controller Down Button Down
        public static bool WasControllerDownBtnDown(PlayerIndex CurIndex)
        {
            return (LastGamePadState[(int)CurIndex].DPad.Down == ButtonState.Pressed);
        }

        // Left Stick Position
        public static Vector2 ControllerLeftStick(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].ThumbSticks.Left;
        }

        // Right Stick Position
        public static Vector2 ControllerRightStick(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].ThumbSticks.Right;
        }

        // Left Stick Position Relative to last position
        public static Vector2 ControllerLeftStickRelative(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].ThumbSticks.Left - LastGamePadState[(int)CurIndex].ThumbSticks.Left;
        }

        // Right Stick Position Relative to last position
        public static Vector2 ControllerRightStickRelative(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].ThumbSticks.Right - LastGamePadState[(int)CurIndex].ThumbSticks.Right;
        }

        // Left Trigger Position
        public static float ControllerLeftTrigger(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].Triggers.Left;
        }

        // Right Trigger Position
        public static float ControllerRightTrigger(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].Triggers.Right;
        }

        // Left Trigger Position Relative to last position
        public static float ControllerLeftTriggerRelative(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].Triggers.Left - LastGamePadState[(int)CurIndex].Triggers.Left;
        }

        // Right Trigger Position Relative to last position
        public static float ControllerRightTriggerRelative(PlayerIndex CurIndex)
        {
            return CurrentGamePadState[(int)CurIndex].Triggers.Right - LastGamePadState[(int)CurIndex].Triggers.Right;
        }

        // Set the vibration of the controller 
        // Motor speeds should be between 0 and 1
        public static void ControllerSetVibration(PlayerIndex CurIndex, float LeftMotor, float RightMotor)
        {
            GamePad.SetVibration(CurIndex, LeftMotor, RightMotor);
        }
    }
}
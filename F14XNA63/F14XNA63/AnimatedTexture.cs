using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace F14
{
    class AnimatedTexture : SceneItem
    {
        public int FrameCount;

        private int currentFrame = 0;
        public bool Repeat = false;
        public bool HasEnded = false;
        public bool skipAutoFrame = false;

        public AnimatedTexture(Game aGame, GraphicsDevice device, Random r, Texture2D tex2d, TextureType tt, int frames)
            : base(aGame, device, r, tex2d, tt)
        {
            FrameCount = frames;
        }

        public override void Reset()
        {
            base.Reset();
            if (!skipAutoFrame)
                TargetRate = .05f;
            currentFrame = 0;            
            HasEnded = false;       
            base.Frame = this.GetFrame(1);
        }

        protected override void UpdateEx(float elapsed, GameTime agt)
        {
            if (!skipAutoFrame)
                this.Frame = GetNextFrame(elapsed);
            else
                base.UpdateEx(elapsed, agt);
            
        }

        /// <summary>
        /// returns next frame to render based on fps.
        /// </summary>
        /// <param name="elapsed"></param>
        /// <returns></returns>
        public Rectangle GetNextFrame(float elapsed)
        {
            if (HasReachedTarget())
            {
                currentFrame++;
                if (currentFrame > FrameCount)
                {
                    if (Repeat)
                        currentFrame = 0;
                    else
                        currentFrame = -1;
                }
            }
            if (currentFrame >= 0 && currentFrame < FrameCount)
            {
                return GetFrame(currentFrame);
            }
            else
            {
                base.HasFinished = true;
                this.isDestroyed = true;
                return GetFrame(1);
            }
        }
        public Rectangle GetFrame(int Frame)
        {
            int FrameWidth = T2D.Width / FrameCount;
            return new Rectangle(FrameWidth * Frame, 0,
                FrameWidth, T2D.Height);
        }
    }
}

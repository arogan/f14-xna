using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XNAExtras;
using F14.Common;

namespace F14
{

    class Director
    {
        public Player p1;
        public int score;

        private Random rand;
        private Game myGame;
        private GameTime myTime;
        private GraphicsDevice device;
        private BitmapFont bFont;
        private Color fontColor = Color.Yellow;
        private SceneCollection clouds = new SceneCollection();
        private SceneCollection Enemies = new SceneCollection();
        private SceneCollection Explosions = new SceneCollection();
        private Dictionary<string, Texture2D> TextureCache = new Dictionary<string, Texture2D>();
        private TextureCache textureCache;
        private System.Collections.Generic.List<ScriptItem> Script = new List<ScriptItem>();
        private System.Collections.Generic.List<ScriptItem> ScriptText = new List<ScriptItem>();
        private int screenwidth, screenheight;
        private Rectangle actionSafeArea, titleSafeArea;
        private ScrollingBackground bg1;
        private ScrollingBackground bg2;
        private int scriptIndex = 0;
        private float globalSpeedV = 0f;
        private float globalSpeedH = 0f;
        private double gtt = 0d;
        private bool isGameOver = false;
        private int HighScore = 0;
        private string version = "v0.61";
        private bool playedGameOverVoice = false;        

        private delegate ScriptItem CreateEnemy(float aStart, int aX);



        public Director(Game aGame, GameTime aTime, GraphicsDevice aDevice, Random aRand, BitmapFont aBf)
        {
            myGame = aGame;
            myTime = aTime;
            device = aDevice;
            rand = aRand;
            bFont = aBf;
            //screenheight = aGame.Window.ClientBounds.Height;
            //screenwidth = aGame.Window.ClientBounds.Width;
            screenheight = aDevice.PresentationParameters.BackBufferHeight;
            screenwidth = aDevice.PresentationParameters.BackBufferWidth;
#if XBOX360
            int dy = (int)(0.05 * screenheight);
            int dx = (int)(0.05 * screenwidth);
            actionSafeArea = new Rectangle(dx, dy, screenwidth - 2 * dx, screenheight - 2 * dy);
            dy = (int)(0.1 * screenheight);
            dx = (int)(0.1 * screenwidth);
            titleSafeArea = new Rectangle(dx, dy, screenwidth - 2 * dx, screenheight - 2 * dy);
#else
            actionSafeArea = new Rectangle(0, 0, screenwidth, screenheight);
            int dy = 10;
            int dx = 10;
            titleSafeArea = new Rectangle(dx, dy, screenwidth - 2 * dx, screenheight - 2 * dy);
#endif
        }

        private void initCollections()
        {
            clouds.MaxItems = 10;
            clouds.TargetRate = .2f;
            Enemies.MaxItems = 30;
            Explosions.MaxItems = Enemies.MaxItems + 10;
        }

        #region text

        public void drawText(Color aColor, int x, int y, string str, float aStart, float aStop, bool isCentered)
        {
            if (gtt >= aStart & gtt <= aStop)
            {
                if (isCentered)
                    drawTextCenter(aColor, str);
                else
                    drawText(aColor, x, y, str);
            }
        }

        public void drawText(Color aColor, int x, int y, string str)
        {
            bFont.KernEnable = true;
            int nWidth = bFont.MeasureString(str);
            bFont.DrawString(x, y, aColor, str, nWidth);
        }

        public void drawText(int x, int y, string str)
        {
            drawText(fontColor, x, y, str);
        }

        public void drawTextCenter(Color aColor, string str)
        {
            bFont.KernEnable = true;
            int nWidth = bFont.MeasureString(str);
            bFont.DrawString((screenwidth - nWidth) / 2, (screenheight - bFont.LineHeight) / 2, aColor, str, nWidth);
        }

        public void drawTextCenter(string str)
        {
            drawTextCenter(fontColor, str);
        }
        #endregion

        #region init
        public void LoadResources(ContentManager content)
        {
            initCollections();
            //load all textures in this single cache
            textureCache = new TextureCache(device, content);

            //player
            p1 = new Player(myGame, device, rand, textureCache.GetTexture(TextureType.f14), TextureType.f14,textureCache.GetTexture(TextureType.f14Side));            
            Projectile p1p = new Projectile(myGame, device,rand,
                textureCache.GetTexture(TextureType.projectilePhoenix),
                TextureType.projectilePhoenix,p1,Sounds.DoubleMachineGunFire,false);
            p1p.Player1 = p1;
            p1p.TargetRate = .15f;
            p1p.SpeedH = 0f;
            p1p.SpeedV = -7f; 
            p1p.MaxProjectiles = 20;
            p1.AlphaProjectiles.Add(p1p);
            p1.Reset();

            //background
            bg1 = new ScrollingBackground(myGame, device, textureCache.GetTexture(TextureType.bgCity1), textureCache.GetTexture(TextureType.bgCity2), textureCache.GetTexture(TextureType.bgCity3));            
            bg1.Layer = 1f;
            bg2 = new ScrollingBackground(myGame, device, textureCache.GetTexture(TextureType.bgClouds));
            bg2.speed = 2;
            bg2.Layer = .99f;            

            //individual clouds            
            for (int i = 0; i < clouds.MaxItems; i++)
            {
                AnimatedTexture aItem = new AnimatedTexture(myGame, device, rand, textureCache.GetTexture(TextureType.bgCloudSheet), TextureType.bgCloudSheet, 4);
                aItem.hasCollision = false;
                aItem.MovePattern = SceneItem.moveType.VLine;
                aItem.VisibleCheck = SceneItem.visibleType.BottomOnly;
                aItem.skipAutoFrame = true;
                clouds.Add(aItem);
            }       

            //enemy                                             
            for (int i = 0; i < Enemies.MaxItems; i++)
            {
                Enemy en = new Enemy(myGame, device, rand, textureCache.GetTexture(TextureType.harrierWhite), TextureType.harrierWhite, p1);                
                en.VisibleCheck = SceneItem.visibleType.BottomOnly;
                Enemies.Add(en);
            }

            //Explosion exp1 = new Explosion(myGame, device, 0, 0);
            for (int i = 0; i < Explosions.MaxItems; i++)
            {
                AnimatedTexture explosion = new AnimatedTexture(myGame, device, rand, textureCache.GetTexture(TextureType.explosionReal), TextureType.explosionReal, 10);
                Explosions.Add(explosion);
            }
            Sound.Update();
            GenerateScript();
        }

        public void Restart()
        {
            Reset(false);
        }

        public void Reset()
        {
            Reset(true);
        }

        private void Reset(bool doFullReset)
        {
            foreach (Enemy en in Enemies)
            {
                en.DestroyAllProjectiles();
                en.IsDestroyed = true;
            }
            scriptIndex = 0;
            
            GenerateScript();
            gtt = 0d;
            bg1.speed = 1;
            bg2.speed = 2;
            Sound.Update();

            if (doFullReset)
            {
                playedGameOverVoice = false;
                isGameOver = false;
                score = 0;
                p1.lives = p1.totalLives;
                p1.Reset();
            }
        }      
        #endregion

        #region script
        public void GenerateScript()
        {
            float sTime = 0f;
            Script.Clear();
            ScriptText.Clear();

            float iSlow = 1.25f;
            float iNormal = 1f;
            float iFast = .75f;
            float iVFast = .25f;
            float bgAccl = .5f;
            float tTime = 0f;

            sTime++;            
            scriptSound(sTime, Sounds.getready);
            sTime = ScriptMsg(sTime, 4f, Color.LightGreen, "Get Ready!");
            

            globalSpeedV = -2f;
            sTime = addSquad(new CreateEnemy(ScriptVLine), sTime, iFast, 10, -1);
            globalSpeedV = 0f;
            sTime = addSquad(new CreateEnemy(ScriptSweepLeft), sTime, iNormal, 5, NormalX(75));
            sTime = addSquad(new CreateEnemy(ScriptSweepRight), sTime, iNormal, 5, NormalX(25));
            globalSpeedV = -1f;
            sTime = addSquad(new CreateEnemy(ScriptAccelerate), sTime, iNormal, 10, -1);
            globalSpeedV = .5f;
            addSquad(new CreateEnemy(ScriptSweepLeft), sTime, iNormal, 5, NormalX(75));
            sTime = addSquad(new CreateEnemy(ScriptSweepRight), sTime, iNormal, 5, NormalX(25));
            globalSpeedV = -1f;
            sTime += 5f;
            sTime = addSquad(new CreateEnemy(ScriptHeliSin), sTime, iFast, 20, -2);
            globalSpeedV = -2f;
            sTime += 8f;
            globalSpeedV = 5f;
            sTime = addSquad(new CreateEnemy(ScriptSin), sTime, iFast, 10, -2);
            globalSpeedV = 0f;
            sTime = addSquad(new CreateEnemy(ScriptSin), sTime, iFast, 10, -2);
            globalSpeedV = -2f;
            sTime = addSquad(new CreateEnemy(ScriptVLine), sTime, iVFast, 30, -1);
            sTime += 10f;
            globalSpeedV = 0f;

            //warning
            tTime = sTime;
            scriptSound(sTime, Sounds.warning2);
            sTime = ScriptMsg(sTime, 1f, Color.Red, "Warning!");
            scriptSound(sTime, Sounds.warning2);
            sTime = ScriptMsg(sTime, 1f, Color.Red, "Warning!");
            scriptSound(sTime, Sounds.warning2);
            sTime = ScriptMsg(sTime, 1f, Color.Red, "Warning!");

            //accelerate backgrounds
            for (float i = 2; i < 8; i++)
            {
                ScriptBg(tTime, i);
                tTime += bgAccl;
            }
            sTime += 3f;
            //boss
            sTime = addSquad(new CreateEnemy(ScriptUfo), sTime, iNormal, 1, NormalX(30));

            Script.Sort();


        }

        private float addSquad(CreateEnemy ce, float start, float interval, int count, int x)
        {
            bool useRand = false;
            bool useRand2 = false;
            if (x == -1)
                useRand = true;
            if (x == -2)
                useRand2 = true;
            for (int i = 0; i < count; i++)
            {
                if (useRand)
                    x = GetRandomX();
                if (useRand2)
                    x = GetRandomXmid();
                Script.Add(ce(start, x));
                start += interval;
            }
            return start;
        }

        private ScriptItem ScriptX(float start)
        {
            ScriptItem si = new ScriptItem();

            return si;
        }

        private float ScriptMsg(float start, float dur, Color aColor, string msg)
        {
            ScriptItem si = new ScriptItem();

            si.Action = ScriptItem.ActionType.WriteText;
            si.Start = start;
            si.Stop = start + dur;
            si.MsgAttrib.IsCentered = true;
            si.MsgAttrib.FColor = aColor;
            si.MsgAttrib.Msg = msg;
            ScriptText.Add(si);

            return si.Stop;
        }

        private void scriptSound(float start, Sounds snd)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.PlaySound;
            si.Start = start;
            si.Sound = snd;
            Script.Add(si);
        }

        private void ScriptBg(float start, float speed)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.Background;
            si.Start = start;
            si.BG1Speed = speed;
            si.BG2Speed = speed + 1;
            Script.Add(si);
        }

        private ScriptItem ScriptHeliSin(float start, int x)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.AddEnemy;
            si.Start = start;
            si.EnemyAttrib.TexType = TextureType.hind;
            si.EnemyAttrib.Rotate = 180f;
            si.EnemyAttrib.MovePattern = SceneItem.moveType.Sin;            
            si.EnemyAttrib.DoRotate = true;
            si.EnemyAttrib.X = x;            
            si.EnemyAttrib.SpeedH = 2f + globalSpeedH;
            si.EnemyAttrib.SpeedV = 2f + globalSpeedV;
            si.EnemyAttrib.Points = 10;
            si.EnemyAttrib.VisibleCheck = SceneItem.visibleType.BottomOnly;
            si.EnemyAttrib.BBSize = SceneItem.bbType.largest;
            
            ScriptItem.ProjectileStruct ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectileMissle;
            ps.IsAimShot = true;
            ps.Scale = 1f;            
            ps.SpeedH = 0f;
            ps.SpeedV = 5f + globalSpeedV;
            ps.Rof = .05f;
            ps.MaxProjectiles = 3;
            ps.ShootSound = Sounds.PeashooterFire;

            si.EnemyAttrib.Projectiles = new ScriptItem.ProjectileStruct[1];
            si.EnemyAttrib.Projectiles[0] = ps;            
            
            return si;
        }

        private ScriptItem ScriptSin(float start, int x)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.AddEnemy;
            si.Start = start;
            si.EnemyAttrib.TexType = TextureType.harrierGreen;
            si.EnemyAttrib.Rotate = 180f;
            si.EnemyAttrib.MovePattern = SceneItem.moveType.Sin;            
            si.EnemyAttrib.DoRotate = false;
            si.EnemyAttrib.X = x;                        
            si.EnemyAttrib.SpeedH = 2f + globalSpeedH;
            si.EnemyAttrib.SpeedV = 2f + globalSpeedV;
            si.EnemyAttrib.Points = 5;
            si.EnemyAttrib.VisibleCheck = SceneItem.visibleType.BottomOnly;
            si.EnemyAttrib.BBSize = SceneItem.bbType.largest;

            ScriptItem.ProjectileStruct ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectileBall;
            ps.IsAimShot = false;
            ps.Scale = 2f;
            ps.SpeedH = 5f;
            ps.SpeedV = 5f + globalSpeedV;
            ps.Rof = .75f;
            ps.MaxProjectiles = 5;
            ps.ShootSound = Sounds.PeashooterFire;

            si.EnemyAttrib.Projectiles = new ScriptItem.ProjectileStruct[1];
            si.EnemyAttrib.Projectiles[0] = ps;      
            return si;
        }

        private ScriptItem ScriptAccelerate(float start, int x)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.AddEnemy;
            si.Start = start;
            si.EnemyAttrib.TexType = TextureType.harrierGreen;
            si.EnemyAttrib.Rotate = 180f;
            si.EnemyAttrib.MovePattern = SceneItem.moveType.Accelerate;            
            si.EnemyAttrib.DoRotate = false;
            si.EnemyAttrib.X = x;                        
            si.EnemyAttrib.SpeedH = 0f + globalSpeedH;
            si.EnemyAttrib.SpeedV = 1f + globalSpeedV;
            si.EnemyAttrib.Points = 5;
            si.EnemyAttrib.VisibleCheck = SceneItem.visibleType.BottomOnly;
            si.EnemyAttrib.BBSize = SceneItem.bbType.largest;

            ScriptItem.ProjectileStruct ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectileBall;
            ps.IsAimShot = true;
            ps.Scale = 2f;
            ps.SpeedH = 0f;
            ps.SpeedV = si.EnemyAttrib.SpeedV + 2f;
            ps.Rof = 1f;
            ps.MaxProjectiles = 10;
            ps.ShootSound = Sounds.PeashooterFire;

            si.EnemyAttrib.Projectiles = new ScriptItem.ProjectileStruct[1];
            si.EnemyAttrib.Projectiles[0] = ps;

            return si;
        }

        private ScriptItem ScriptSweepRight(float start, int x)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.AddEnemy;
            si.Start = start;
            si.EnemyAttrib.TexType = TextureType.harrierGreen;
            si.EnemyAttrib.Rotate = 180f;
            si.EnemyAttrib.MovePattern = SceneItem.moveType.SweepRight;            
            si.EnemyAttrib.DoRotate = false;
            si.EnemyAttrib.X = x;              
            si.EnemyAttrib.SpeedH = 0f + globalSpeedH;
            si.EnemyAttrib.SpeedV = 2f + globalSpeedV;          
            si.EnemyAttrib.Points = 2;
            si.EnemyAttrib.VisibleCheck = SceneItem.visibleType.BottomOnly;
            si.EnemyAttrib.BBSize = SceneItem.bbType.largest;

            ScriptItem.ProjectileStruct ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectileBall;
            ps.IsAimShot = false;
            ps.Scale = 2f;
            ps.SpeedH = 0f;
            ps.SpeedV = si.EnemyAttrib.SpeedV + 4f;
            ps.Rof = 1f;
            ps.MaxProjectiles = 10;
            ps.ShootSound = Sounds.PeashooterFire;

            si.EnemyAttrib.Projectiles = new ScriptItem.ProjectileStruct[1];
            si.EnemyAttrib.Projectiles[0] = ps;            

            return si;
        }

        private ScriptItem ScriptSweepLeft(float start, int x)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.AddEnemy;
            si.Start = start;
            si.EnemyAttrib.TexType = TextureType.harrierGreen;
            si.EnemyAttrib.Rotate = 180f;
            si.EnemyAttrib.MovePattern = SceneItem.moveType.SweepLeft;
            si.EnemyAttrib.X = x;
            si.EnemyAttrib.SpeedH = 0f + globalSpeedH;
            si.EnemyAttrib.SpeedV = 2f + globalSpeedV;   
            si.EnemyAttrib.Points = 2;
            si.EnemyAttrib.VisibleCheck = SceneItem.visibleType.BottomOnly;
            si.EnemyAttrib.BBSize = SceneItem.bbType.largest;

            ScriptItem.ProjectileStruct ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectileBall;
            ps.IsAimShot = false;
            ps.Scale = 2f;
            ps.SpeedH = 0f;
            ps.SpeedV = si.EnemyAttrib.SpeedV + 4f;
            ps.Rof = 1f;
            ps.MaxProjectiles = 10;
            ps.ShootSound = Sounds.PeashooterFire;

            si.EnemyAttrib.Projectiles = new ScriptItem.ProjectileStruct[1];
            si.EnemyAttrib.Projectiles[0] = ps;      

            return si;
        }

        private ScriptItem ScriptVLine(float start, int x)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.AddEnemy;
            si.Start = start;
            si.EnemyAttrib.TexType = TextureType.harrierWhite;
            si.EnemyAttrib.Rotate = 180f;
            si.EnemyAttrib.MovePattern = SceneItem.moveType.VLine;            
            si.EnemyAttrib.DoRotate = false;
            si.EnemyAttrib.X = x;                        
            si.EnemyAttrib.SpeedH = 0f + globalSpeedH;
            si.EnemyAttrib.SpeedV = 3.5f + globalSpeedV;
            si.EnemyAttrib.VisibleCheck = SceneItem.visibleType.BottomOnly;
            si.EnemyAttrib.BBSize = SceneItem.bbType.largest;

            ScriptItem.ProjectileStruct ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectileBall;
            ps.IsAimShot = true;
            ps.Scale = 2f;
            ps.SpeedH = 0f;
            ps.SpeedV = si.EnemyAttrib.SpeedV + 4f + globalSpeedV;
            ps.Rof = 2f;            
            ps.MaxProjectiles = 10;
            ps.ShootSound = Sounds.PeashooterFire;

            si.EnemyAttrib.Projectiles = new ScriptItem.ProjectileStruct[1];
            si.EnemyAttrib.Projectiles[0] = ps;            

            return si;
        }

        private ScriptItem ScriptUfo(float start, int x)
        {
            ScriptItem si = new ScriptItem();
            si.Action = ScriptItem.ActionType.AddEnemy;
            si.Start = start;
            si.EnemyAttrib.TexType = TextureType.ufo;
            si.EnemyAttrib.Rotate = 0f;
            si.EnemyAttrib.MovePattern = SceneItem.moveType.BounceBox;            
            si.EnemyAttrib.DoRotate = false;
            si.EnemyAttrib.X = x;                        
            si.EnemyAttrib.SpeedH = 1f + globalSpeedH;
            si.EnemyAttrib.SpeedV = 1f + globalSpeedV;                        
            si.EnemyAttrib.Health = 60;            
            si.EnemyAttrib.Points = 100;
            si.EnemyAttrib.VisibleCheck = SceneItem.visibleType.BottomOnly;
            si.EnemyAttrib.BBSize = SceneItem.bbType.smallest;

            ScriptItem.ProjectileStruct ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectilePlasma;
            ps.IsAimShot = true;
            ps.Scale = 1f;
            ps.SpeedH = 0f;
            ps.SpeedV = si.EnemyAttrib.SpeedV + 2f;
            ps.Rof = .1f;            
            ps.MaxProjectiles = 10;
            ps.ShootSound = Sounds.PeashooterFire;

            si.EnemyAttrib.Projectiles = new ScriptItem.ProjectileStruct[3];
            si.EnemyAttrib.Projectiles[0] = ps;

            ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectileBall;
            ps.IsAimShot = true;
            ps.Scale = 2f;
            ps.SpeedH = 0f;
            ps.SpeedV = si.EnemyAttrib.SpeedV + 3f;
            ps.Rof = 1.25f;
            ps.MaxProjectiles = 5;
            ps.ShootSound = Sounds.PeashooterFire;
            si.EnemyAttrib.Projectiles[1] = ps;

            ps = new ScriptItem.ProjectileStruct();
            ps.TexType = TextureType.projectileFireball;
            ps.IsAimShot = true;
            ps.Scale = 1.2f;
            ps.SpeedH = 0f;
            ps.SpeedV = si.EnemyAttrib.SpeedV + 4f;
            ps.Rof = 3.5f;
            ps.MaxProjectiles = 1;
            ps.ShootSound = Sounds.PeashooterFire;
            si.EnemyAttrib.Projectiles[2] = ps;

            return si;
        }

        protected int NormalY(float YPercent)
        {
            return actionSafeArea.Top + Convert.ToInt32(actionSafeArea.Height * (YPercent / 100f));
        }

        protected int NormalX(float XPercent)
        {
            return actionSafeArea.Left + Convert.ToInt32(actionSafeArea.Width * (XPercent / 100f));
        }

        private int GetRandomX()
        {
            return rand.Next(actionSafeArea.Left + 50, actionSafeArea.Right - 50);
        }

        private int GetRandomXmid()
        {
            return rand.Next(NormalX(25), screenwidth - NormalX(25));
        }

        #endregion

        #region update
        public void Update(float elapsed, GameTime agt)
        {
            gtt += elapsed;

            //player
            p1.Update(elapsed, agt);

            //backgrounds
            bg1.Update(elapsed);
            bg2.Update(elapsed);

            //individual clouds            
            clouds.Update(elapsed, agt);
            if (clouds.HasReachedTarget())
            {
                foreach (AnimatedTexture si in clouds)
                {
                    if (si.IsDestroyed)
                    {
                        //create a new individual cloud    
                        si.Reset();
                        si.Frame = si.GetFrame(rand.Next(0, si.FrameCount + 1));
                        si.Pos.X = rand.Next(0, screenwidth);
                        si.Pos.Y = -si.Frame.Height;
                        si.SpeedV = rand.Next(Convert.ToInt32(bg2.speed) + 1, Convert.ToInt32(bg2.speed) + 8);                                              
                        si.Layer = .98f;                                                
                        break;
                    }
                }
            }

            //script items
            int startIndex = scriptIndex;
            for (int i = startIndex; i < Script.Count; i++)
            {
                ScriptItem s = Script[i];
                if (gtt >= s.Start)
                {
                    switch (s.Action)
                    {
                        case ScriptItem.ActionType.AddEnemy:
                            foreach (Enemy en in Enemies)
                            {
                                if (en.IsDestroyed && en.AreAllProjectilesDestoryed())
                                {                                    
                                    en.T2D = textureCache.GetTexture(s.EnemyAttrib.TexType);
                                    en.BBSize = s.EnemyAttrib.BBSize;                                                                        
                                    en.SpeedH = s.EnemyAttrib.SpeedH;
                                    en.SpeedV = s.EnemyAttrib.SpeedV;
                                    en.Pos.X = s.EnemyAttrib.X;
                                    en.Pos.Y = -en.Height + s.EnemyAttrib.Y;                                    
                                    en.Rotate = MathHelper.ToRadians(s.EnemyAttrib.Rotate);
                                    if (s.EnemyAttrib.Health > 0)
                                        en.Health = s.EnemyAttrib.Health;
                                    else
                                        en.Health = 1;
                                    if (s.EnemyAttrib.Points > 0)
                                        en.Points = s.EnemyAttrib.Points;
                                    en.DoRotate = s.EnemyAttrib.DoRotate;
                                    en.MovePattern = s.EnemyAttrib.MovePattern;
                                    en.VisibleCheck = s.EnemyAttrib.VisibleCheck;

                                    //projectile
                                    en.AlphaProjectiles.Clear();
                                    foreach (ScriptItem.ProjectileStruct ps in s.EnemyAttrib.Projectiles)
                                    {
                                        Projectile p = new Projectile(myGame, device, rand, 
                                            textureCache.GetTexture(ps.TexType), ps.TexType, 
                                            en, ps.ShootSound, ps.IsAimShot);
                                        p.SpriteScale = ps.Scale;                                        
                                        p.SpeedH = ps.SpeedH;
                                        p.SpeedV = ps.SpeedV;
                                        p.PosOffset.X = ps.PosOffsetX;
                                        p.PosOffset.Y = ps.PosOffsetY;
                                        p.TargetRate = ps.Rof;
                                        p.MaxProjectiles = ps.MaxProjectiles;
                                        p.Player1 = p1;
                                        en.AlphaProjectiles.Add(p);
                                    }                                    
                                    en.Reset();
                                    break;
                                }
                            }
                            break;
                        case ScriptItem.ActionType.Background:
                            bg1.speed = s.BG1Speed;
                            bg2.speed = s.BG2Speed;
                            break;
                        case ScriptItem.ActionType.PlaySound:
                            Sound.Play(s.Sound);
                            break;
                    }
                    scriptIndex++;
                }
                else
                {
                    break;
                }
            }

            //check to see if you finished the level
            if (!isGameOver && scriptIndex >= Script.Count && Enemies.IsAllDestroyed())            
            {
                Restart();
            }

            if (isGameOver && !playedGameOverVoice)
            {
                Sound.Play(Sounds.gameover);
                playedGameOverVoice = true;
            }

            Enemies.Update(elapsed, true, agt);
            Explosions.Update(elapsed, agt);
            CheckCollisions();
        }

        private void CheckCollisions()
        {
            //collision detection
            //shoot enemies
            foreach (SceneCollection sc in p1.GroupProjectiles)
            {
                foreach (Projectile mp in sc)
                {
                    foreach (Enemy e1 in Enemies)
                    {
                        if ((!e1.IsDestroyed) && (!mp.IsDestroyed) && mp.BB.Intersects(e1.BB))
                        {
                            e1.Health--;
                            if (e1.Health <= 0)
                            {                                                             
                                //draw explosion
                                mp.IsDestroyed = true;
                                createExplosion(e1);
                                updateScore(e1.Points);                                
                                break;
                            }
                            else
                            {                              
                                createExplosion(mp);                             
                            }
                        }
                    }
                }
            }

            //check for collision against player
            foreach (Enemy e1 in Enemies)
            {
                if (!e1.IsDestroyed && !p1.IsDestroyed && p1.lifeTime > 2f)
                {
                    //planes intersect
                    if (e1.BB.Intersects(p1.BB))
                    {
                        e1.Health--;
                        if (e1.Health <= 0)
                        {
                            updateScore(e1.Points);
                            createExplosion(e1);
                        }
                        createExplosion(p1);
                        isGameOver = p1.DoDestroy();                        
                        break;
                    }
                }

                //shot by enemy bullet
                foreach (SceneCollection sc in e1.GroupProjectiles)
                {
                    foreach (Projectile b in sc)
                    {
                        if (!p1.IsDestroyed && !b.IsDestroyed && b.BB.Intersects(p1.BB) && p1.lifeTime > 2f)
                        {
                            b.IsDestroyed = true;
                            createExplosion(p1);
                            isGameOver = p1.DoDestroy();
                            break;
                        }
                    }
                }
            }
        }

        private void updateScore(int pts)
        {
            score += pts;
            if (score > HighScore)
                HighScore = score;
        }

        private void createExplosion(SceneItem item)
        {
            try
            {
                if (item.Width > item.Height)
                    createExplosion(item, item.Width / Explosions[0].Frame.Width);
                else
                    createExplosion(item, item.Height / Explosions[0].Frame.Height);
            }
            catch (System.Exception ex)
            {
                createExplosion(item, 2f);
            }

        }

        private void createExplosion(SceneItem item, float aScale)
        {
            item.IsDestroyed = true;
            if (aScale == 0f)
                aScale = 1f;
            //draw explosion
            foreach (AnimatedTexture exp in Explosions)
            {
                //find one not in use
                if (exp.IsDestroyed)
                {
                    if (item is Player)
                        Sound.Play(Sounds.ExplodeShip);
                    else
                        Sound.Play(Sounds.RocketExplode);
                    exp.Pos = item.Pos;
                    exp.SpriteScale = aScale;
                    exp.Reset();
                    break;
                }
            }            
        }

        private void createExplosion(Vector2 aPos, float aScale)
        {          
            if (aScale == 0f)
                aScale = 1f;
            //draw explosion
            foreach (AnimatedTexture exp in Explosions)
            {
                //find one not in use
                if (exp.IsDestroyed)
                {                   
                    Sound.Play(Sounds.RocketExplode);
                    exp.Pos = aPos;
                    exp.SpriteScale = aScale;
                    exp.Reset();
                    break;
                }
            }
        }
        #endregion

        public void Draw(SpriteBatch Batch)
        {
            bg1.Draw(Batch);
            bg2.Draw(Batch);

            clouds.Draw(Batch);
            Enemies.Draw(Batch);
            Explosions.Draw(Batch);

            //text
            drawText(titleSafeArea.Left, titleSafeArea.Top, "Score: " + score.ToString());
            drawText(screenwidth / 2, titleSafeArea.Top, "High Score: " + HighScore.ToString());
            drawText(titleSafeArea.Left, titleSafeArea.Bottom - 20, version.ToString());

#if XBOX360
            if (isGameOver)
                drawTextCenter("Press Start to restart or Back to Quit.");
#else
            if (isGameOver)
                drawTextCenter("Press Home to restart or Esc to Quit.");
#endif

            //scrip text items
            foreach (ScriptItem s in ScriptText)
            {
                this.drawText(s.MsgAttrib.FColor, s.MsgAttrib.X, s.MsgAttrib.Y, s.MsgAttrib.Msg, s.Start, s.Stop, s.MsgAttrib.IsCentered);
            }

            //player
            p1.Draw(Batch);       
        }
    } //class
} //namespace

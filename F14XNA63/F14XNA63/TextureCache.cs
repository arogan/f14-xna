using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XNAExtras;
using F14.Common;

namespace F14
{

    public enum TextureType
    {
        harrierWhite,
        harrierGreen,
        hind,
        ufo,
        projectileBall,
        projectileBulletUp,
        projectileBulletDown,
        projectileFireball,
        projectilePlasma,
        projectileMissle,
        projectilePhoenix,
        explosionAnime,
        explosionReal,
        bgCloudSheet,
        bgCity1,
        bgCity2,
        bgCity3,
        bgClouds,
        f14,
        f14Side
    }

	class TextureCache
	{
        private static string[] textureNames = new string[]
        {
            @"Content\Sprites\harrier_s.png",
            @"Content\Sprites\harrier2_s.png",
            @"Content\Sprites\hind_s.png",
            @"Content\Sprites\ufo.png",
            @"Content\Sprites\bullet.dds",
            @"Content\Sprites\bullet_s.png",
            @"Content\Sprites\bullet_s2.png",
            @"Content\Sprites\fireball_s.png",
            @"Content\Sprites\plasma_s.png",
            @"Content\Sprites\missle2.png",
            @"Content\Sprites\phoenix_s.png",
            @"Content\Sprites\explosion.dds",
            @"Content\Sprites\explosion_s.png",
            @"Content\Sprites\clouds_sheet3.png",
            @"Content\Backgrounds\city1.jpg",
            @"Content\Backgrounds\city2.jpg",
            @"Content\Backgrounds\city3.jpg",
            @"Content\Backgrounds\clouds3.png",
            @"Content\Sprites\f14sheet_s.png",
            @"Content\Sprites\f14side.png"
        };

        private Texture2D tex;
        
        private Dictionary<string, Texture2D> tc = new Dictionary<string, Texture2D>();

        public TextureCache(GraphicsDevice device, ContentManager content)
        {            
            foreach (string s in textureNames)
            {
                string name = s.Remove(s.Length - 4, 4);
                tex = content.Load<Texture2D>(name);
                tc.Add(s, tex);
            }
        }

        public Texture2D GetTexture(TextureType tt)
        {
            return tc[textureNames[(int)tt]];
        }
	}
}

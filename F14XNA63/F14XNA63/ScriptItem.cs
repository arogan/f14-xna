using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XNAExtras;
using F14.Common;
using System.Collections;

namespace F14
{
    class ScriptItem : IComparable<ScriptItem>
    {
        public enum ActionType
        {
            AddEnemy,
            WriteText,
            PlaySound,
            Background,
        }
        public ActionType Action;

        //global
        public float Start;
        public float Stop;
        public Sounds Sound;
        public float BG1Speed = 1f;
        public float BG2Speed = 2f;

        //text
        public struct MsgStruct
        {
            public string Msg;
            public int X;
            public int Y;
            public bool IsCentered;
            public Color FColor;
        }
        public MsgStruct MsgAttrib;

        //projectile attributes
        public struct ProjectileStruct
        {
            public TextureType TexType;
            public Sounds ShootSound;            
            public float Scale;
            //public float Speed;
            public float SpeedH;
            public float SpeedV;
            public float PosOffsetX;
            public float PosOffsetY;
            public bool IsAimShot;
            public float Rof;     
            public int MaxProjectiles;
            
        }

        //enemy attributes
        public struct EnemyStruct
        {
            //public float Speed;
            public float SpeedH;
            public float SpeedV;
            public TextureType TexType;
            public float X;
            public float Y;                   
            public bool DoRotate;
            public float Rotate;
            public float Scale;                      
            public int Health;
            public SceneItem.moveType MovePattern;
            public SceneItem.visibleType VisibleCheck;                    
            public int Points;
            public SceneItem.bbType BBSize;
            public ProjectileStruct[] Projectiles;
        }
        public EnemyStruct EnemyAttrib;

        public int CompareTo(ScriptItem obj)
        {
            if (obj == null) return 1;

            if (this.Start == obj.Start)
            {
                return 0;
            }
            else if (this.Start > obj.Start)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

    }
}

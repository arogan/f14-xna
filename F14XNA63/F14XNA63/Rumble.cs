using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SimpleInput;

namespace F14
{
    class Rumble : SceneItem
    {
        public float Durration = 0f;

        public Rumble(Game aGame, GraphicsDevice device, float adur)
            : base(aGame, device)
        {
            Durration = adur;
            HasFinished = true;
        }

        protected override void UpdateEx(float elapsed, GameTime agt)
        {
            if (this.lifeTime <= Durration)
                IM.ControllerSetVibration(PlayerIndex.One, .5f, .5f);
            else
            {
                base.HasFinished = true;
                IM.ControllerSetVibration(PlayerIndex.One, 0, 0);
            }
        }
    }
}
